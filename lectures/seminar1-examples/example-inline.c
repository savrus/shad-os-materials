#include <stdio.h>
int main() {
    int v1, v2, v3, *v4;
    asm ("movl $10, %0": "=rm" (v1));
    printf("%d\n", v1);
    asm ("movl %1, %0" : "=r" (v2) : "m" (v1));
    printf("%d\n", v2);
    asm ("popcnt %1, %0" : "=r" (v3) : "m" (v2));
    printf("%d\n", v3);
    v4 = &v3;
    asm ("movl (%1), %0" : "=r" (v1) : "r" (v4));
    printf("%d\n", v1);
}
