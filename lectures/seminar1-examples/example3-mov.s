.global s
s:
movb $0xff, %al          # записать 8-битную константу в AL
movb $0xff, %r8b         # записать 8-битную константу в R8L
movw $0xffff, %bx        # записать 16-битную константу в BX
movl $0xffffffff, %edi   # записать 32-битную константу в EDI
movq $0xffffffffffffffff, %r15   #  64-битную константу в R15

movb %al, %bh            # скопировать 8-битный AL в BH
movw %bx, %r8w           # скопировать 16-битный BX в R8X
movl %r10d, %esp         # скопировать 32-битный R10D в ESP      
movq %rax, %rbp          # скопировать 64-битный RAX в RBP
movb %al, 0x12345        # записать значение AL по адресу 0x12345

movb $0xff, (%rsp)           # записать 8-битную константу по адресу в регистре %rsp
movw %bx, 10(%rax)           # записать 16-битную константу по адресу (%rax + 10)
movl 9(%rax, %rcx), %esi     # прочитать 32-битное значение по адресу (%rax + %rcx + 9) в %esi
movq 3(%r10, %rax, 4), %r14  # прочитать 64-битное значение по адресу (4 * %rsp + %r10 + 9) в %r16


movl %eax, %ebx
movq %rax, (%rsp, %r11)
movb 0xff, %ah
movb $0xff, %r10b
movl $0xff, 4(%rbp, %rbx)
movq $-0x1, 8(%rax, %rax, 4)


# записать 8-битное значение AH
# в 32-битный EAX расширив нулями
movzbl %ah, %eax

# записать 8-битное значение по адресу (%rps)
# в 16-битный BX расширив нулями
movzbw (%rsp), %bx

# записать 16-битное значение AX
# в 32-битный EAX расширив знаково
movswl %ax, %eax 
# записать 32-битное значение по адресу (%rax)
# в 64-битный RAX расширив знаково
movslq (%rax), %rax 

movzwq (%rsp), %r15
movsbw %ch, %dx


# сложить 5 и EDI и записать результат в EDI
addl $5, %edi

# сложить R8 и R10 и записать результат в R10
addq %r8, %r10

# сложить BH и значение по адресу 10, записав туда же результат
addb %bh, 0x10

# сложить AX и значение по адресу (%rax + %rax), результат в AX
addw (%rax, %rax), %ax

# вычесть из ESP EAX, записать результат в ESP
subl %eax, %esp# вычесть из ESP EAX, записать результат в ESP
subl %eax, %esp

# вычесть из значения по адресу (%rax) R8
# результат записать в R8
subq %r8, (%rax)

# вычесть из значения по адресу (%rax) R8
# результат записать в R8
subq %r8, (%rax)

# записать %rsp + %rcs в EAX
leal (%rsp, %rcx), %eax

# записать %rax + 4 * %rcx + 0x123 в %r8
leaq 0x123(%rax, %rcx, 4), %r8


# сдвинуть значение в EDI на 1 влево
shll $1, %edi

# сдвинуть беззнаково значение в BX вправо
# на число бит, записанное в CL
shrw %cl, %bx

# знаково сдвинуть значение по адресу (%r8)
# на 3 бита вправо
sarq $3, (%r8)

# оставить в CL только один младший бит
andb $1, %cl

# выполнить логическое ИЛИ значений BX и по адресу (%rax)
# резузльтат записать по адресу (%rax)
orw %bx, (%rax)

# выполнить исключающее ИЛИ значений EAX и по адресу (%rsp + 4)
# результат записать в EAX
xorl 4(%rsp), %eax

begin:
cmpl %eax, %ebx
jbe target

cmpb %al, %ah
jg target

testb %al, %al
jne target

testq $0x2, %rsp
jz target

target:
jmp begin


movq %rax, %fs:(%rbx)

