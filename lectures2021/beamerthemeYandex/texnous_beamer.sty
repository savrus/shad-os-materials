%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                                      %
%  Common definitions for TeXnous beamer theme.                                                                        %
%  XeLaTeX or LuaLaTeX compilator needed.                                                                              %
%                                                                                                                      %
%  Author: Kirill Chuvilin <k.chuvilin@texnous.org>, 2017                                                              %
%  Copyright: 2017 Kirill Chuvilin                                                                                     %
%  License: BSD-3-Clause                                                                                               %
%                                                                                                                      %
%  Redistribution and use in source and binary forms, with or without modification, are permitted provided that        %
%  the following conditions are met:                                                                                   %
%                                                                                                                      %
%  1. Redistributions of source code must retain the above copyright notice, this list of conditions and               %
%  the following disclaimer.                                                                                           %
%                                                                                                                      %
%  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and            %
%  the following disclaimer in the documentation and/or other materials provided with the distribution.                %
%                                                                                                                      %
%  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote     %
%  products derived from this software without specific prior written permission.                                      %
%                                                                                                                      %
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,  %
%  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE   %
%  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  %
%  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS        %
%  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY              %
%  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY     %
%  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                             %
%                                                                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\RequirePackage{currfile}% monitor include paths
\RequirePackage{polyglossia}% localization and fonts
\RequirePackage{scrextend}% horizontal margins
\RequirePackage{framed}% framed boxes
\RequirePackage{listings}% listings


\def\@texnous@releaseDate{2017/10/06}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Environment processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newif\if@texnous@portable% portable usage flag
\ifx\currfiledir\empty% if this file is included from texmf
	\@texnous@portablefalse% not portable usage
\else% if this file is included from subdirectory
	\@texnous@portabletrue% portable usage
\fi%

\let\@texnous@resourceDir\currfiledir% the resources directory
\let\@texnous@fontDir\currfiledir% directory with fonts



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newif\if@texnous@russian% true if current language is Russian
\@texnous@russiantrue% Russian by default
\newif\if@texnous@logoFrame% true to insert the first frame with logo
\@texnous@logoFrametrue% insert by default
\newif\if@texnous@nda% true to mark the slides as NDA
\@texnous@ndafalse% not NDA by default
\newif\if@texnous@useUnicodeMath% true to require unicode-math package
\@texnous@useUnicodeMathtrue% use by default

\def\texnous@processOptions{%
	\DeclareOption{russian}{% use Russian language
		\@texnous@russiantrue%
	}
	\DeclareOption{english}{% use English language
		\@texnous@russianfalse%
	}
	\DeclareOption{logoframe}{% insert the first frame with logo
		\@texnous@logoFrametrue%
	}
	\DeclareOption{nologoframe}{% don't insert the first frame with texnous logo
		\@texnous@logoFramefalse%
	}
	\DeclareOption{nda}{% mark the slides as NDA
		\@texnous@ndatrue%
	}
	\DeclareOption{notnda}{% mark the slides as NDA
		\@texnous@ndafalse%
	}
	\DeclareOption{unicodemath}{% use unicode math
		\@texnous@useUnicodeMathtrue%
	}
	\DeclareOption{nounicodemath}{% don't use unicode math
		\@texnous@useUnicodeMathfalse%
	}
	\ProcessOptions\relax%
	\if@texnous@russian%
		\setdefaultlanguage{russian}%
		\patchcmd{\theorem}{Theorem}{Теорема}{}{}%
		\patchcmd{\lemma}{Lemma}{Лемма}{}{}%
		\def\@texnous@contactsTitle{Контакты}%
	\else%
		\setdefaultlanguage{english}%
		\def\@texnous@contactsTitle{Contacts}%
	\fi%
	\def\texnous@processOptions{}% process options only once
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some LaTeX magic and render options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% Check for argument in curved brackets
% \texnous@checkargument[#1]#2  argument in curved brackets
% \texnous@checkargument*[#1]#2 argument in square brackets
% #1 - default value for the argument
% #2 - code to execute after (optional arg will be available as #1)
%%
\def\texnous@checkArgument{\@ifstar\texnous@checkArgument@@\texnous@checkArgument@}%
\newcommand{\texnous@checkArgument@}[2][]{%
	\def\texnous@checkArgument@worker##1{#2}%
	\@ifnextchar\bgroup{\texnous@checkArgument@worker}{\texnous@checkArgument@worker{#1}}%
}
\newcommand{\texnous@checkArgument@@}[2][]{%
	\def\texnous@checkArgument@worker[##1]{#2}%
	\@ifnextchar[{\texnous@checkArgument@worker}{\texnous@checkArgument@worker[#1]}%
}


%%
% Box environemt
% \begin{texnous@box}[#1]#2#3[#4][#5]...\end{texnous@box}
% #1 - vertical alignment
% #2 - width
% #3 - height
% #4 - left padding
% %5 - right padding
%%
\newenvironment{texnous@box}[3][c]{% 
	\ifvmode% if in vertical mode
		\@texnous@box@vmodetrue%
		\edef\@texnous@backup@baselineskip{\the\baselineskip}% store baseline height
		\fontsize{\f@size}{0pt}\selectfont% reset baseline height
	\else% if in horizontal mode
		\@texnous@box@vmodefalse%
	\fi%
	\texnous@checkArgument*[0pt]{% read optional left padding
		\setlength\@texnous@box@leftPadding{##1}%
		\texnous@checkArgument*[0pt]{% read optional right padding
			\setlength\@texnous@box@rightPadding{####1}%
			\setlength\textwidth{#2}%
			\advance\textwidth by-\@texnous@box@leftPadding%
			\advance\textwidth by-\@texnous@box@rightPadding%
			\hspace{\@texnous@box@leftPadding}%
			\ifx&#3&% if height isn't set
				\begin{minipage}[b]{\textwidth}%
			\else% if height is set
				\setlength\textheight{#3}%
				\begin{minipage}[b][\textheight][#1]{\textwidth}%
			\fi%
			\if@texnous@box@vmode% if the baseline height was reseted
				\fontsize{\f@size}{\@texnous@backup@baselineskip}\selectfont% restore baseline height locally
			\fi%
		}%
	}%
}{%
	\end{minipage}%
	\hspace{\@texnous@box@rightPadding}%
	\if@texnous@box@vmode% if the box was in the vertical mode
		\vskip0pt% go back to the vertical mode
	\fi%
}
\newlength\@texnous@box@leftPadding%
\newlength\@texnous@box@rightPadding%
\newif\if@texnous@box@vmode%


\def\ps@texnous@clear{% pagestyle without footer and header
	\setbeamertemplate{headline}{}%
	\setbeamertemplate{footline}{}%
	\setbeamertemplate{frametitle}{}%
}


\def\ps@texnous@nofootline{% pagestyle without footer
	\setbeamertemplate{footline}{}%
}


%%
% Remove header, foorter and horizontal margins from the frame
% [#1] - vertical alignment
%%
\newenvironment{texnous@clearFrame}[1][c]{% 
	\thispagestyle{texnous@clear}% page withoute footer and header
	\def\texnous@resetFrame{% go to the top and start the frame again
			\end{minipage}%
		\end{addmargin}%
		\vspace{-\paperheight}% go to the frame top
		\begin{addmargin}[-\beamer@leftmargin]{-\beamer@rightmargin}% remove horizontal margins
			\vskip-1pt%
			\begin{minipage}[b][\paperheight][#1]{\linewidth}%
				\textheight=\paperheight%
				\ignorespaces%
	}%
	\begin{addmargin}[-\beamer@leftmargin]{-\beamer@rightmargin}% remove horizontal margins
		\vskip-1pt%
		\vskip-\headheight\vskip-\headsep% go back to the frame top
		\begin{minipage}[b][\paperheight][#1]{\linewidth}%
			\textheight=\paperheight%
			\ignorespaces%
}{%
		\end{minipage}%
	\end{addmargin}%
	\vskip-\footheight% go back to the frame bottom
	\ignorespaces%
}


%%
% Suppress output
% Via: https://tex.stackexchange.com/a/97360	
%%
\newenvironment{texnous@suppress}{%
	\parskip\z@%
	\offinterlineskip%
	\baselineskip=\z@skip%
	\lineskip=\z@skip%
	\lineskiplimit=\maxdimen%
	\@texnous@dummyfont%
	\count@\sixt@@n%
	\loop\ifnum\count@ >\z@%
		\advance\count@\m@ne%
		\textfont\count@\@texnous@dummyfont%
		\scriptfont\count@\@texnous@dummyfont%
		\scriptscriptfont\count@\@texnous@dummyfont%
	\repeat%
	\let\selectfont\relax%
	\let\mathversion\@gobble%
	\let\getanddefine@fonts\@gobbletwo%
	\tracinglostchars\z@%
	\frenchspacing%
	\hbadness\@M%
	\renewcommand{\includegraphics}[2][]{}%
}{%
}
\font\@texnous@dummyfont=dummy\relax%


\lstset{%
	inputencoding=utf8x,
	breakatwhitespace=false, % sets if automatic breaks should only happen at whitespace
	breaklines=true,         % sets automatic line breaking
	captionpos=n,            % no caption
	keepspaces=true,         % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	showspaces=false,        % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,  % underline spaces within strings only
	showtabs=false,          % show tabs within strings adding particular underscores
}


\lstdefinelanguage{JavaScript}{% JS for listings
	keywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break},
	ndkeywords={class, export, boolean, throw, implements, import, this},
	sensitive=false,
	comment=[l]{//},
	morecomment=[s]{/*}{*/},
	morestring=[b]',
	morestring=[b]"
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sizes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% Get TeXnous length
% \texnouslenght#1
% #1 - the length key
%%
\newcommand\texnouslength[1]{\csname @texnous@length@#1\endcsname}%


%%
% Register new TeXnous length
% \newtexnouslength[#1]#2
% #1 - the start value for the length
% #2 - the length key
%%
\newcommand{\newtexnouslength}[2][0pt]{%
	\expandafter\newlength\csname @texnous@length@#2\endcsname% register new length
	\define@key{texnous@length}{#2}{\expandafter\setlength\csname @texnous@length@#2\endcsname{##1}}% register new key to set the lengths
	\expandafter\setlength\csname @texnous@length@#2\endcsname{#1}% set the start value
}


\newcommand{\settexnoussize}[1]{%
	\setkeys{texnous@length}{#1}%
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand\TeXnous{\TeX{nous}\ignorespaces}


%%%
%% Define footline text
%% #1 the footline text
%%%
\newcommand<>{\footline}[1]{\only#2{%
	\ifbeamer@inframe% if in frame
		\gdef\@texnous@footline{#1}% redefine local footline text
	\else% if not in frame
		\gdef\@texnous@globalFootline{#1}% redefine global footline text
	\fi%
}}
\def\@texnous@footline{}% local footline text storage
\def\@texnous@globalFootline{}% global footline text storage


%%%
%% Insert footline text
%%%
\newcommand\insertfootline{
	\ifx\@texnous@footline\empty% if local footline text isn't defined
		\@texnous@globalFootline% insert global footline
	\else% if local footline text is defined
		\@texnous@footline% insert it
		\gdef\@texnous@footline{}% and reset it
	\fi%
}


%%
% Quote wrapper
% #1 width of the vertical line
% #2 width of the text padding
% #3 top padding
% #4 bottom padding
%%
\newenvironment{@texnous@quote}[4]{%
	\def\FrameCommand{\usebeamercolor[bg]{quote}\vrule width #1\hspace{#2}\usebeamercolor[fg]{quote}}%
	\MakeFramed{\advance\hsize-\width\FrameRestore}%
		\vspace{#3}%
		\edef\@texnous@quote@paddingBottom{#4}%
}{%
		\vspace{\@texnous@quote@paddingBottom}%
	\endMakeFramed%
}



\endinput
